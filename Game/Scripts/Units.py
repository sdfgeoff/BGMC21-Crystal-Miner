'''Handles unit operations'''
import bge
import mathutils

import logging
import random

import defs
import Mouse
import Control
import Buildings
import Sounds

LOG = logging.getLogger()


def init_units(_):
    '''Adds the units to the scheduler'''
    bge.logic.scheduler.register_function(update_units)
    bge.logic.unit_list = list()


def update_units():
    '''Runs every frame to update the path of each unit'''
    pass
    # for unit in bge.logic.unit_list:
    #     unit.update()


def assign_target(event, pos, selected, hover):
    '''Assigns the target for all selected units'''
    if event is None:
        return None
    mouse_node = pos
    if event[defs.MOUSE_MAP['MOVE']] != 0:
        for unit in bge.logic.unit_list:
            if unit.selected:
                unit.set_target_node(mouse_node)

    if event[defs.MOUSE_MAP['SELECT']] == 1:
        if Control.get_key('LEFTSHIFTKEY') == 0:
            for unit in bge.logic.unit_list:
                unit.select(False)
            Mouse.set_selected(None)
        select_unit(event, pos, selected, hover)


def deselect_all_units():
    '''Deselects all units'''
    for unit in bge.logic.unit_list:
        unit.select(False)


def select_unit(event, pos, selected, hover):
    '''Selects the unit under the mouse'''
    if event is None:
        return

    if event[defs.MOUSE_MAP['SELECT']] == bge.logic.KX_SENSOR_JUST_ACTIVATED:
        if isinstance(hover, Unit):
            hover.select(True)
            Mouse.set_selected(assign_target)


def add_unit(obj_type, spawn_obj):
    '''Spawns the unit at the supplied object and mutates it'''
    obj_name = obj_type['obj_name']
    level = obj_type['level']
    obj = spawn_obj.scene.objectsInactive[obj_name]
    new_unit = spawn_obj.scene.addObject(obj, spawn_obj)
    new_unit = UNIT_MUTATE_DICT[obj_type['unit_type']](new_unit, level)
    bge.logic.unit_list.append(new_unit)


class Unit(bge.types.KX_GameObject):
    '''Base class for a generic unit'''
    def __init__(self, _):
        self.selected = False
        self.speed = 0
        self.target_node = None
        self.direction_indicator = DirectionIndicator()
        self.selection_scale = [0.5, 0.5]
        self.selection_indicator = SelectionIndicator(self)
        self.definite_target_node = None
        bge.logic.scheduler.register_function(self.update)
        self.selection_indicator.set_scale([1, 1])
        self.sound = Sounds.EngineSound(self)

        self.specs = defs.DEFAULT_UNIT

    def pathfind_to(self, target_node):
        '''Pathfinds to a location'''
        if target_node is None:
            return None
        unit_pos = bge.logic.map.convert_to_map_coords(self.worldPosition)
        path = bge.logic.map.navigate_between(unit_pos, target_node, self)
        if path is None:
            self.direction_indicator.set_color(0.0)
        else:
            self.direction_indicator.set_color(0.3)
            delta = mathutils.Vector(list(path[-1])) - \
                unit_pos - mathutils.Vector([0.5, 0.5])
            self.move(-delta)
            return delta

    def set_text(self, designation):
        '''Specifies a unit's identifier'''
        for child in self.childrenRecursive:
            if isinstance(child, bge.types.KX_FontObject):
                child.resolution = 3
                child.text = designation

    def select(self, state):
        '''Sets if a unit is selected or not'''
        self.selected = state
        self.selection_indicator.set_visible(state)

    def set_target_node(self, node):
        '''Sets the position the vehicle is travelling towards'''
        self.target_node = node
        self.definite_target_node = node

    def get_target_node(self):
        '''Returns the position the vehicle is travelling towards'''
        return self.definite_target_node

    def move(self, vec):
        '''Makes the unit attempt to move along target vector'''
        if vec.length > 0.1:
            vec.length = min(vec.length * 2, self.speed)
        else:
            return
        vec3 = list(vec) + [0]
        self.localLinearVelocity.y = vec.length
        self.localLinearVelocity.x = 0
        self.face_direction(vec3)
        if self.worldLinearVelocity.z > 0.1:
            self.worldLinearVelocity.z = 0.001

    def get_terrain(self, cardinal_delta=None):
        '''Returns the terrain the rover is on or in the direction
        it is trying to move if supplied'''
        if cardinal_delta is None:
            cardinal_delta = [0, 0]
        node = bge.logic.map.get_closest_node(self.worldPosition)
        terrain = node.get_terrain()
        if terrain == 0:
            x_pos, y_pos = node.mapPosition
            x_pos = x_pos - cardinal_delta[0]
            y_pos = y_pos - cardinal_delta[1]
            node = bge.logic.map.get_node([x_pos, y_pos])
            terrain = node.get_minable()
        return [terrain, node.mapPosition]

    def update_direction_indicator(self):
        '''Updates the units direction indicator to show it's
        current heading'''
        if self.target_node is not None:
            from_pos = self.worldPosition.copy()
            from_pos.z = 0.1
            to_pos = bge.logic.map.convert_to_map_coords(self.target_node)
            to_pos.resize_3d()
            to_pos.z = 0.1
            self.direction_indicator.set_direction(from_pos, to_pos)

    def face_direction(self, direction):
        '''Makes the unit face in the specified direction'''
        direction = mathutils.Vector(direction)
        if direction[0] != 0 or direction[1] != 0:
            self.alignAxisToVect(direction, 1, 0.2)
            self.alignAxisToVect([0, 0, 1], 2)

            delta = direction.normalized() - self.getAxisVect([0, -1, 0])
            if delta.length < 0.01:  # Want to go the opposite direction
                self.applyRotation([0, 0, 0.1], False)


class MinerUnit(Unit):
    '''The level 1 and 2 mining units'''

    def __init__(self, obj, level):
        super().__init__(obj)
        self.set_text('M{:02d}'.format(random.randint(0, 99)))
        self.collected_resources = {'Ore': 0, 'Crystal': 0}

        self.specs = defs.MINING_VEHICLE_LEVELS[level]

    def check_placables(self):
        '''Interacts with placables at current position'''
        node = bge.logic.map.get_closest_node(self.worldPosition)
        placables = node.get_contains()
        if len(placables) > 0:
            for placable in placables:
                if placable.get('Droppable', False):
                    placable.add_resources(self.collected_resources)
                    for resource in self.collected_resources:
                        self.collected_resources[resource] = 0
                        self.target_node = self.definite_target_node
                if placable.get('SmoothSurface', False) is not False:
                    self.speed = self.specs['smooth_speed']
                else:
                    self.speed = self.specs['rough_speed']

        else:
            self.speed = self.specs['rough_speed']

    def mine_map(self, pos, direction):
        '''Mines the specified position and faces the specified direction'''
        if not self.check_full():
            mine_speed = self.specs['mining_speed']
            mine_dir = [pos[0] + direction[0], pos[1] + direction[1]]
            collected = bge.logic.map.mine(mine_dir, mine_speed)
            if collected is None:
                return False
            for resource in collected:
                self.collected_resources[resource] += collected[resource]
            return True
        else:
            return False

    def get_resources_quantity(self):
        '''Returns the total count of resources stored in the unit'''
        total = 0
        for resource in self.collected_resources:
            total += self.collected_resources[resource]
        return total

    def get_percent_full(self):
        '''Returns the state of fullness of the vehicle'''
        return self.get_resources_quantity() / self.specs['resource_limit']

    def check_full(self):
        '''Returns true if the unit is too full to mine any moee'''
        return self.get_resources_quantity() > self.specs['resource_limit']

    def update(self):
        '''Update this units mining, pathfinding etc.'''
        if self.target_node is not None:
            if self.check_full():
                node = bge.logic.map.get_closest_node(self.worldPosition)
                dropsite = get_nearest_dropsite(node.mapPosition)
                self.pathfind_to(dropsite)
                self.check_placables()
            else:
                self.check_placables()
                move_direction = self.pathfind_to(self.target_node)
                node = bge.logic.map.get_closest_node(self.worldPosition)
                if node.get_hardness() != 0 and node.get_minable():
                    self.mine_map(node.mapPosition, [0, 0])
                else:
                    delta = convert_to_cardinal(move_direction)
                    self.mine_map(node.mapPosition, delta)

        if self.selected:
            selection_text = "{}%".format(min(100, int(self.get_percent_full() * 100)))
            self.selection_indicator.set_text(selection_text)
            self.selection_indicator.update_direction()
            if self.target_node is not None:
                self.direction_indicator.set_visible(True)
                self.update_direction_indicator()
            else:
                self.direction_indicator.set_visible(False)
        else:
            self.direction_indicator.set_visible(False)


class ResourceUnit(Unit):
    '''A unit that allows mining vehicles to deposit resources into
    itself so they don't have to travel as far'''
    def __init__(self, obj, level):
        super().__init__(obj)
        self.set_text('M{:02d}'.format(random.randint(0, 99)))
        self.collected_resources = {'Ore': 0, 'Crystal': 0}

        self.specs = defs.RESOURCE_VEHICLE_LEVELS[level]
        self['Droppable'] = True

    def get_resources_quantity(self):
        '''Returns the total count of resources stored in the unit'''
        total = 0
        for resource in self.collected_resources:
            total += self.collected_resources[resource]
        return total

    def get_percent_full(self):
        '''Returns the state of fullness of the vehicle'''
        return self.get_resources_quantity() / self.specs['resource_limit']

    def check_full(self):
        '''Returns true if the unit is too full to mine any moee'''
        return self.get_resources_quantity() > self.specs['resource_limit']

    def add_resources(self, collected):
        '''Adds resources to the resource unit'''
        for resource in collected:
            self.collected_resources[resource] += collected[resource]

    def set_target_node(self, node):
        '''Sets the position the vehicle is travelling towards.
        Overwritten so that can remove it's placable'''
        if self.target_node is not None:
            target_node = bge.logic.map.get_node(self.target_node)
            target_node.remove_object(self)
        self.target_node = node
        self.definite_target_node = node

    def check_placables(self):
        '''Interacts with placables at current position'''
        node = bge.logic.map.get_closest_node(self.worldPosition)
        placables = node.get_contains()
        if len(placables) > 0:
            for placable in placables:
                if placable.get('Droppable', False) and placable is not self:
                    placable.add_resources(self.collected_resources)
                    for resource in self.collected_resources:
                        self.collected_resources[resource] = 0
                        self.target_node = self.definite_target_node
                if placable.get('SmoothSurface', False) is not False:
                    self.speed = self.specs['smooth_speed']
                else:
                    self.speed = self.specs['rough_speed']

        else:
            self.speed = self.specs['rough_speed']

    def update(self):
        '''Runs every time the resource truck needs to be updated'''
        if self.target_node is not None:
            if not self.check_full():
                self.pathfind_to(self.target_node)
                unit_node = bge.logic.map.get_closest_node(self.worldPosition)
                target_node = bge.logic.map.get_node(self.target_node)
                if target_node == unit_node and self not in target_node.get_contains():
                    target_node.add_object(self)
            else:
                self.pathfind_to(get_home_base())
                target_node = bge.logic.map.get_node(self.target_node)
                target_node.remove_object(self)

        if self.selected:
            self.selection_indicator.update_direction()
            selection_text = "{}%".format(min(100, int(self.get_percent_full() * 100)))
            self.selection_indicator.set_text(selection_text)
            if self.target_node is not None:
                self.direction_indicator.set_visible(True)
                self.update_direction_indicator()
            else:
                self.direction_indicator.set_visible(False)
        else:
            self.direction_indicator.set_visible(False)

        self.check_placables()


class DirectionIndicator(object):
    '''Returns an object that can be stretched to be used as a direction
    indicator'''
    def __init__(self):
        scene = bge.logic.getCurrentScene()
        self.obj = scene.addObject(defs.DIRECTION_INDICATOR_OBJECT,
                                   scene.objects[0])
        self.obj.worldOrientation = [0, 0, 0]

    def set_direction(self, from_point, to_point):
        '''Sets the position and direction of the indicator using a
        start and end point'''
        self.obj.worldPosition = from_point
        direction_vect = (from_point - to_point)
        self.obj.worldScale = [1, direction_vect.length, 1]
        self.obj.alignAxisToVect(-direction_vect, 1)
        self.obj.alignAxisToVect([0, 0, 1], 2)
        self.obj.color[0] -= 0.1
        self.obj.color[1] = 1 / direction_vect.length

    def set_visible(self, state):
        '''Sets visibility of indicator'''
        self.obj.visible = state

    def set_color(self, hue):
        '''Sets the color of the direction indicator'''
        self.obj.color[2] = hue


def get_nearest_dropsite(pos):
    '''Returns the nearest resource drop point'''
    if isinstance(pos, list):
        pos = mathutils.Vector([pos[0], pos[1]])

    drop_points = []
    drop_points.append(get_home_base())

    for unit in bge.logic.unit_list:
        if isinstance(unit, ResourceUnit):
            drop_points.append(unit.get_target_node())

    min_distance = 99
    selected = None
    for drop_point in drop_points:
        if drop_point is None:
            continue
        distance = (drop_point - pos).length
        if distance < min_distance:
            selected = drop_point
            min_distance = distance

    return selected


def get_home_base():
    '''Returns the position of home base'''
    return Buildings.get_entry().get_drop_position()


def convert_to_cardinal(direction):
    '''Converts a direction into one of 9 possible directions'''
    if direction is None:
        return [0, 0]
    if not isinstance(direction, mathutils.Vector):
        direction = mathutils.Vector(direction)
    direction.length = min(1, direction.length)
    out = [round(direction.x), round(direction.y)]
    return out


class SelectionIndicator(object):
    '''Returns an object that can be stretched to be used as a direction
    indicator'''
    def __init__(self, parent):
        scene = bge.logic.getCurrentScene()
        self.obj = scene.addObject(defs.UNIT_SELECTION_OBJECT, parent)
        self.set_visible(False)
        self.obj.worldOrientation = [0, 0, 0]
        self.set_scale(parent.selection_scale)
        self.parent = parent
        self.obj.setParent(self.parent)

    def update_direction(self):
        '''Changes the direction to face the camera'''
        scene = bge.logic.getCurrentScene()
        self.obj.worldOrientation = scene.active_camera.worldOrientation

    def set_scale(self, scale):
        '''Sets the scale of the indicator'''
        self.obj.worldScale = [scale[0], scale[1], 1]

    def set_text(self, text):
        '''Sets the text on the selection indicator'''
        for child in self.obj.childrenRecursive:
            if isinstance(child, bge.types.KX_FontObject):
                child.text = text

    def set_visible(self, state):
        '''Sets visibility of indicator'''
        self.obj.visible = state
        for child in self.obj.childrenRecursive:
            child.visible = state

    def set_color(self, rgb):
        '''Sets the color of the selection indicator'''
        self.obj.color = rgb


UNIT_MUTATE_DICT = {
    'Mine': MinerUnit,
    'Resource': ResourceUnit,
}

'''Handles all map operations'''

import bge
import mathutils
import logging
import random
import math

import Common
import defs
import Loader
import DynTex
import Buildings
import Navigation


LOG = logging.getLogger()
SEED = 56478


def generate_list_of_lists(size):
    '''Generates a list of lists of a specified size, duh'''
    tmp = list()
    for row in range(0, size):
        tmp.append(list())
        for column in range(0, size):
            tmp[row].append(list())
            tmp[row][column] = 0
    return tmp


def generate_map(_):
    '''Iteratively generates the map. Returns True when finished'''
    if 'map' not in generate_map.__dict__:
        bge.logic.map = GlobalMap(defs.WORLD_SIZE, random.randint(0, 10000))
        generate_map.map = bge.logic.map

    bge.logic.scheduler.register_function(check_game_complete, time_between=1)
    function_list = bge.logic.map.load_functions
    num = len(function_list)
    count = generate_map.__dict__.get('count', 0)
    if count < num:
        Loader.display_load_message('Generating Map {}/{}'.format(count, num))
        LOG.debug("Function: %s", function_list[count])
        function_list[count]()
        generate_map.count = count + 1
        return False
    del generate_map.__dict__['map']
    return True


def add_crystal(pos):
    '''Adds a crystal'''
    scene = Common.get_scene(defs.GAME_SCENE_NAME)
    crystal = scene.addObject('Crystal', scene.objects[0])
    crystal.worldPosition = pos
    crystal.worldPosition.z += 0.18
    crystal.worldScale = [1, 1, 1]
    crystal.worldOrientation = [0, 0, 0]
    crystal['crystal_resource'] = float(defs.CRYSTAL_PER_CRYSTAL)
    return crystal


def add_tile(shape, position, world_size=0):
    '''Adds a specific shape tile in the specified position. Examples
    of shapes are [[1,0][0,0]], [[0,1],[0,1]]'''
    scene = Common.get_scene(defs.GAME_SCENE_NAME)

    shape_dict = COMPLETE_SHAPE_DICT
    shape = list(list(x) for x in shape)
    for row in [0, 1]:
        for col in [0, 1]:
            if shape[row][col] != 0:
                shape[row][col] = 1
    hashable_shape = tuple(tuple(x) for x in shape)
    if hashable_shape in shape_dict:
        shape_name, num_rots = shape_dict[hashable_shape]
        if shape_name not in ['Floor', 'Roof']:
            tile_type = get_tile_type(shape_name)
            new_tile = scene.addObject(tile_type, scene.objects[0])

            new_tile.worldScale = [1, 1, 1]
            new_tile.worldPosition = position
            new_tile.applyMovement([-0.5, -0.5, 0])
            new_tile.worldOrientation = [0, 0, num_rots * math.pi / 2]
            new_tile.color = [world_size / 255, 0.0, 0.0, 1.0]

            return new_tile
    else:
        LOG.error("Attempted to add %s", str(hashable_shape))


class MapNode(object):
    '''Represents a single point in the map'''
    def __init__(self, position, terrain):
        self.contains = []
        self.terrain = terrain
        self.pos = position
        self.terrain_obj = None

    def get_speed(self, unit):
        '''Returns the speed a unit will cross this node'''
        if unit is None:
            unit_specs = defs.DEFAULT_UNIT
            unit_is_full = True
        else:
            unit_specs = unit.specs
            unit_is_full = unit.check_full()
        if self.get_minable() or self.on_border():
            if unit_is_full:
                mining_rate = 0
            else:
                mining_rate = unit_specs['mining_speed'] * defs.MINE_RATE
            if mining_rate != 0:
                terrain_distance = self.get_hardness()
                return float(unit_specs['rough_speed'] +
                             mining_rate / terrain_distance)
            else:
                return 0.0
        else:
            speed = 10000000000
            for placeable in self.get_contains():
                if placeable.get('Blocks', False):
                    speed = min(speed, 0.0)
                elif placeable.get('SmoothSurface', False):
                    speed = min(float(unit_specs['smooth_speed']*10), speed)
            if speed != 10000000000:
                return speed
            return float(unit_specs['rough_speed']*10)

    def get_contains(self):
        '''Returns the objects contained by the node'''
        return self.contains

    def set_terrain_object(self, obj):
        '''Sets the object used to display the object'''
        self.terrain_obj = obj

    def get_terrain_object(self):
        '''Returns the object used to display the object'''
        return self.terrain_obj

    def set_terrain(self, terrain):
        '''Sets the terrain for the node'''
        self.terrain = terrain

    def get_terrain(self):
        '''Returns the terrain at the node'''
        if self.on_border():
            return 9
        return self.terrain

    def on_border(self):
        '''Returns True if the node is on the border of the map'''
        if self.pos[0] == 0 or self.pos[0] == bge.logic.map.size or \
            self.pos[1] == 0 or self.pos[0] == bge.logic.map.size:
            return True
        else:
            return False

    def add_object(self, thing):
        '''Adds an object to the node'''
        self.contains.append(thing)

    def remove_object(self, thing):
        '''Removes an object from the node'''
        if thing in self.contains:
            self.contains.remove(thing)

    def get_hardness(self):
        '''Returns hardness of the terrain.'''
        if self.terrain != 0:
            return self.terrain
        for obj in self.contains:
            if 'crystal_resource' in obj:
                return defs.CRYSTAL_PER_CRYSTAL * defs.CRYSTAL_MINE_BUFF
        return 0

    def get_minable(self):
        '''Returns true if terrain is minable'''
        if self.on_border():
            return False
        if self.terrain != 0:
            return True
        for obj in self.contains:
            if 'crystal_resource' in obj:
                return True
        return False

    def mine(self, speed):
        '''Mines resources from the node'''
        if self.on_border():
            return {}
        mine_quantity = defs.MINE_RATE / bge.logic.getAverageFrameRate()
        mine_quantity *= speed
        if self.terrain != 0:
            mine_quantity = min(mine_quantity, self.terrain)
            prev_amount = self.get_terrain()
            amount = prev_amount - mine_quantity
            self.set_terrain(amount)

            total_value = 0
            for val_range in defs.ORE_GATHER_RATE:
                start_val, stop_val = val_range
                if amount > start_val and amount < stop_val:
                    ore = defs.ORE_GATHER_RATE[val_range] * mine_quantity
                    total_value += ore
            return {'Ore': total_value}
        for obj in self.contains[:]:
            if 'crystal_resource' in obj:
                mine_quantity *= defs.CRYSTAL_MINE_BUFF
                available_crystal = obj['crystal_resource']
                mine_quantity = min(mine_quantity, available_crystal)
                amount = available_crystal - mine_quantity
                obj['crystal_resource'] = amount

                if amount == 0:
                    obj.endObject()
                    self.contains.remove(obj)

                return {'Crystal': mine_quantity}

    def __str__(self):
        return "Node: ({},{})".format(*self.mapPosition)

    @property
    def worldPosition(self):
        '''Returns the world position of the node'''
        x_pos = defs.WORLD_SIZE / 2 - self.pos[0] + 0.5
        y_pos = defs.WORLD_SIZE / 2 - self.pos[1] + 0.5
        return mathutils.Vector([x_pos, y_pos, 0])

    @property
    def mapPosition(self):
        '''Returns the position of the node in the map'''
        return self.pos


class GlobalMap(object):
    '''Creates a much more versitile map than the previous map'''
    BEDROCK = 9
    ROCK = 2
    DIRT = 1
    EMPTY = 0

    def __init__(self, size, seed=SEED):
        mathutils.noise.seed_set(seed)
        self.raw = []
        self.size = size
        self.world_tex = None
        self.pathfinding = Navigation.Astar(self)

        self.load_functions = [
            self.generate_blank,
            self.generate_rock,
            self.generate_dirt,
            self.generate_bedrock,
            self.add_floor,
            self.place_entry,
            self.add_crystals,
            self.initial_place_tiles,
        ]

    def navigate_between(self, start_pos, end_pos, unit):
        '''Returns a list of nodes to go through to navigate from the
        start to the end'''
        first_node = self.get_node(make_round_node(start_pos))
        last_node = self.get_node(make_round_node(end_pos))
        if first_node == last_node:
            return [end_pos + mathutils.Vector([0.5, 0.5])]
        else:
            route = self.pathfinding.navigate_between(
                first_node.mapPosition,
                last_node.mapPosition,
                unit
            )
            if route is not None:
                return route[0:-1]
            else:
                return route

    def add_crystals(self):
        '''Places crystals around the map'''
        placed = 0
        iters = 0
        while placed < defs.NUM_CRYSTAL_ATTEMPTS and iters < 20000:
            rand_x = int(mathutils.noise.random() * self.size)
            rand_y = int(mathutils.noise.random() * self.size)
            node = self.get_node([rand_x, rand_y])
            if node.get_terrain() == 0 and node.get_contains() == []:
                pos = node.worldPosition
                crystal = add_crystal(pos)
                node.add_object(crystal)
                placed += 1
            iters += 1

    def initial_place_tiles(self):
        '''Builds the actual map'''
        for x_raw in range(-1, self.size):
            for y_raw in range(-1, self.size):
                node = self.get_node([x_raw, y_raw])
                shape = self.get_shape([x_raw, y_raw], 2)
                added = add_tile(shape, node.worldPosition, self.size)
                node.set_terrain_object(added)
                self.update_dyn_tex([x_raw, y_raw])

    def update_dyn_tex(self, pos):
        '''Updates the texture that shows the map status'''
        if pos[0] >= 0 and pos[0] < self.size \
                and pos[1] >= 0 and pos[1] < self.size:
            val = self.get_node(pos).get_terrain()
            health = int(val / 10 * 255)
            occluded = 255 if val != 0 else 0
            col = [health, occluded, defs.WORLD_SIZE]
            self.world_tex.set_pixel(pos[0], pos[1], col + [255])

    def generate_dirt(self):
        '''Erodes the map to fill in space for dirt'''
        tmp_map = generate_list_of_lists(self.size)
        for x_raw in range(self.size):
            for y_raw in range(self.size):
                top, right, bottom, left = self.get_cardinal([x_raw, y_raw])
                val = max(top.get_terrain(), bottom.get_terrain(),
                          left.get_terrain(), right.get_terrain())
                if val == self.ROCK:
                    tmp_map[x_raw][y_raw] = self.DIRT

        for x_raw in range(self.size):
            for y_raw in range(self.size):
                node = self.get_node([x_raw, y_raw])
                terrain_to_be = max(node.get_terrain(), tmp_map[x_raw][y_raw])
                node.set_terrain(terrain_to_be)

    def generate_rock(self):
        '''Places the metal deposits'''
        for x_raw in range(self.size):
            for y_raw in range(self.size):
                value = mathutils.noise.random()
                if value > defs.METAL_THRESHOLD:
                    self.get_node([x_raw, y_raw]).set_terrain(self.ROCK)
                else:
                    self.get_node([x_raw, y_raw]).set_terrain(self.EMPTY)
        self.amazing_algorithm()
        self.amazing_algorithm()

    def generate_bedrock(self):
        '''Generates the hardest rock: bedrock'''
        for x_raw in range(self.size):
            for y_raw in range(self.size):
                x_pos = x_raw * defs.BEDROCK_SCALE
                y_pos = y_raw * defs.BEDROCK_SCALE
                value = mathutils.noise.noise([x_pos, y_pos, SEED])
                if value > defs.BEDROCK_THRESHOLD:
                    self.get_node([x_raw, y_raw]).set_terrain(self.BEDROCK)

    def amazing_algorithm(self):
        '''Does the CaveX14 algorithm....'''
        for x_raw in range(self.size):
            for y_raw in range(self.size):
                top, right, bottom, left = self.get_cardinal([x_raw, y_raw])
                if top.get_terrain() == bottom.get_terrain() and \
                        left.get_terrain() == right.get_terrain():
                    self.get_node([x_raw, y_raw]).set_terrain(0)

    def generate_blank(self):
        '''Fills the map with empty terrain'''
        for x_pos in range(0, self.size):
            self.raw.append(list())
            for y_pos in range(0, self.size):
                self.raw[x_pos].append(list())
                self.raw[x_pos][y_pos] = MapNode([x_pos, y_pos], 0)

    def add_floor(self):
        '''Adds the cavern floor'''
        scene = Common.get_scene(defs.GAME_SCENE_NAME)
        floor = scene.addObject('Floor', scene.objects[0])
        floor.worldPosition = [1, 1, 0]
        floor.worldScale = [self.size-1] * 2 + [1]
        floor.worldOrientation = [0, 0, 0]
        floor.color = [self.size / 255, 0.0, 0.0, 1.0]
        self.generate_world_tex(floor)

    def place_entry(self):
        '''Places the mine entry'''
        iters = 0
        while iters < 2000:
            rand_x = int(mathutils.noise.random() * defs.WORLD_SIZE)
            rand_y = int(mathutils.noise.random() * defs.WORLD_SIZE)
            position = [rand_x, rand_y]
            if self.get_shape(position, 2) == [[0, 0], [0, 0]]:
                Buildings.place_entry(self.get_node(position).worldPosition)
                LOG.debug("Placed base in %d iterations", iters)
                return
            iters += 1

        LOG.error("Unable to place base nicely. Apply brute force")
        center = int(self.size / 2)
        for x_pos in range(center, center + 2):
            for y_pos in range(center, center + 2):
                print(x_pos, y_pos)
                self.get_node([x_pos, y_pos]).set_terrain(self.EMPTY)

        Buildings.place_entry(self.get_node([center, center]).worldPosition)

    def generate_world_tex(self, floor):
        '''Generates the texture used to obscure the parts of the map
        that are not visible'''
        self.world_tex = DynTex.DynTex(floor, self.size)

    def get_node(self, pos):
        '''Returns the node at a specified point. Must be an integer
        list as a parameter'''
        x_pos, y_pos = pos
        if x_pos >= 0 and x_pos < self.size and \
                y_pos >= 0 and y_pos < self.size:
            return self.raw[math.ceil(x_pos)][math.ceil(y_pos)]
        else:
            return MapNode([x_pos, y_pos], self.BEDROCK)

    def convert_to_map_coords(self, pos):
        '''converts the supplied position into map coordinates, not
        rounding to integer'''
        x_pos = self.size / 2 - pos[0]
        y_pos = self.size / 2 - pos[1]
        return mathutils.Vector([x_pos, y_pos])

    def get_closest_node(self, pos):
        '''Returns the closest node to a specified position. Returns
        None if not in range'''
        x_pos = defs.WORLD_SIZE / 2 - pos[0]
        y_pos = defs.WORLD_SIZE / 2 - pos[1]
        if x_pos >= 0 and x_pos < self.size and \
                y_pos >= 0 and y_pos < self.size:
            return self.raw[math.ceil(x_pos)][math.ceil(y_pos)]
        else:
            return MapNode([x_pos, y_pos], self.BEDROCK)

    def mine(self, pos, speed):
        '''Mines the supplied position or node at the specified speed.
        Returns a dict of the resources extracted'''
        if isinstance(pos, MapNode):
            node = pos
        else:
            node = self.get_node(pos)
        prev_terrain = node.get_terrain()
        resources = node.mine(speed)
        if prev_terrain != 0 and node.get_terrain() == 0:
            self.regenerate_node(node)
        else:
            self.update_dyn_tex(node.mapPosition)
        return resources

    def regenerate_node(self, root_node):
        '''Updates the objects displaying the specified node'''
        root_node_pos = root_node.mapPosition
        for delta_x in range(-1, 1):
            for delta_y in range(-1, 1):
                x_pos = root_node_pos[0] + delta_x
                y_pos = root_node_pos[1] + delta_y
                node = self.get_node([x_pos, y_pos])
                node_obj = node.get_terrain_object()
                if node_obj is not None:
                    node_obj.endObject()

                shape = self.get_shape([x_pos, y_pos], 2)
                pos = node.worldPosition
                added = add_tile(shape, pos, self.size)
                node.set_terrain_object(added)
                self.update_dyn_tex([x_pos, y_pos])

    def get_cardinal(self, pos):
        '''Returns a list of the cardinal directions in order:
        [top right bottom left] (clockwise from top)'''
        x_raw, y_raw = pos
        right = self.get_node([x_raw + 1, y_raw])
        left = self.get_node([x_raw - 1, y_raw])
        top = self.get_node([x_raw, y_raw + 1])
        bottom = self.get_node([x_raw, y_raw - 1])
        return [top, right, bottom, left]

    def get_shape(self, pos, ret_type=0):
        '''Returns the shape of four points near a node.
        If ret_type == 0 (default), it will return the nodes.
        If ret_type == 1, it will return the terrain
        If ret_type == 2, it will return the passability'''
        x_pos, y_pos = pos
        shape = [[self.get_node([x_pos, y_pos + 1]),
                  self.get_node([x_pos + 1, y_pos + 1])],
                 [self.get_node([x_pos, y_pos]),
                  self.get_node([x_pos + 1, y_pos])]]
        if ret_type == 0:
            return shape
        else:
            for x_pos, rows in enumerate(shape):
                for y_pos, cell in enumerate(rows):
                    shape[x_pos][y_pos] = cell.get_terrain()
                    if ret_type == 2:
                        shape[x_pos][y_pos] = math.ceil(shape[x_pos][y_pos])
            return shape

    def __str__(self):
        out_str = ''
        for x_raw in range(self.size):
            for y_raw in range(self.size):
                value = self.get_node([x_raw, y_raw]).get_terrain()
                out_str += str(int(value)) + '  '
            out_str += '\n'
        return out_str


def build_complete_shape_dict():
    '''Reverses SHAPE_DICT so that the object can be found by
    looking up the desired shape'''
    def rotate_shape(shape, num):
        '''Rotates a shape so that you only need one orientation of
        an object in mesh form'''
        new_shape = list(list(x) for x in shape)
        for _ in range(num + 1):
            tmp = new_shape[0][0]
            new_shape[0][0] = new_shape[0][1]
            new_shape[0][1] = new_shape[1][1]
            new_shape[1][1] = new_shape[1][0]
            new_shape[1][0] = tmp
        return tuple(tuple(x) for x in new_shape)

    new_shape_dict = {}
    for raw_shape in SHAPE_DICT:
        for i in range(4):
            shape = rotate_shape(SHAPE_DICT[raw_shape], i)

            new_shape_dict[shape] = [raw_shape, i]

    return new_shape_dict


def make_round_node(pos):
    '''Converts a floating point node into a integer node'''
    return [math.ceil(pos[0]), math.ceil(pos[1])]


def get_tile_type(name):
    '''Returns a random tile of the correct type'''
    scene = Common.get_scene(defs.GAME_SCENE_NAME)
    all_of_type = [o for o in scene.objectsInactive if o.name.startswith(name)]
    return random.choice(all_of_type)


def check_game_complete():
    '''Adds the end game scene if the game is complete'''
    scene = Common.get_scene(defs.GAME_SCENE_NAME)
    num_crystals = len([o for o in scene.objects if 'crystal_resource' in o])
    if num_crystals == 0:
        bge.logic.addScene('EndGameScene')

SHAPE_DICT = {
    'Floor': ((0, 0),
              (0, 0)),

    'Wall': ((0, 1),
             (0, 1)),

    'Split': ((0, 1),
              (1, 0)),

    'Inner Corner': ((0, 1),
                     (1, 1)),

    'Outer Corner': ((0, 0),
                     (0, 1)),

    'Roof': ((1, 1),
             (1, 1)),
}

COMPLETE_SHAPE_DICT = build_complete_shape_dict()

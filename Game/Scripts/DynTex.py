import logging

import bge


LOG = logging.getLogger()

class DynTex(object):
    '''Creates a dynamic texture for an object'''
    def __init__(self, obj, res=512):
        LOG.debug("Creating dynamic texture for %s", obj.name)
        self.obj = obj
        self.res = res
        material = self.obj.meshes[0].materials[0]
        mat_id = bge.texture.materialID(self.obj, str(material))
        tex = bge.texture.Texture(self.obj, mat_id, 0)
        tex.source = bge.texture.ImageBuff(res, res, 0)
        obj['tex'] = tex
        self.tex = tex
        self.bucket_fill([0, 127, 0, 255])
        bge.logic.scheduler.register_function(self.update_tex, time_between=0)
        self.edited = True

    def bucket_fill(self, rgba):
        '''Blankets the whole texture in a colour'''
        for x_pos in range(self.res):
            for y_pos in range(self.res):
                self.set_pixel(x_pos, y_pos, rgba)

    def set_pixel(self, x, y, rgba, blend_mode=bge.texture.IMB_BLEND_COPY):
        '''Sets the value of a single pixel'''
        brush = bytearray(rgba)
        self.tex.source.plot(brush, 1, 1, x, y, blend_mode)
        self.edited = True

    def update_tex(self):
        '''Should be run once per frame at the end of all the edits.
        Don't ask me why'''
        if self.edited is True:
            self.tex.refresh(False)
        self.edited = False

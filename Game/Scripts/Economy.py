'''Handles transactions requireing resources'''
import logging
import defs

LOG = logging.getLogger()


def get_wallet():
    '''Returns the current wallet objects'''
    return WALLET


class Wallet(object):
    '''Stores resources'''
    def __init__(self, resource_types):
        self.resources = dict()
        for resource in resource_types:
            self.resources[resource] = 0
        LOG.debug("Created Wallet")
        self.text_obj = None

    def add_resource(self, resource_dict):
        '''Adds the the resources'''
        for resource in resource_dict:  # Deduct Cost
            assert resource in self.resources
            self.resources[resource] += resource_dict[resource]
        self.update_text()

    def withdraw(self, resource_dict):
        '''Attempts to withdraw the specified quantities'''
        for resource in resource_dict:  # Check available
            assert resource in self.resources
            if self.resources[resource] - resource_dict[resource] < 0:
                return False

        for resource in resource_dict:  # Deduct Cost
            assert resource in self.resources
            self.resources[resource] -= resource_dict[resource]
        self.update_text()
        return True

    def set_text_object(self, game_obj):
        '''Sets the object to be used to display current resources'''
        self.text_obj = game_obj
        self.text_obj.resolution = 5

    def update_text(self):
        '''Updates the display of resources'''
        if self.text_obj is None:
            return
        out_str = ''
        for resource in self.resources:
            quantity = int(self.resources[resource])
            out_str += "{:7}{:5d}\n".format(resource, quantity)

        self.text_obj.text = out_str


WALLET = Wallet(defs.START_RESOURCES.keys())

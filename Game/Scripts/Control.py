'''Handles all form of user input transparently'''
import json
import os
import logging

import bge

import Common
import defs
import Mouse

LOG = logging.getLogger()
SETTINGS = dict()
EVENT_CALLBACKS = dict()
CLICKABLES = dict()


MOUSE_EVENTS = {
    'LEFTMOUSE': bge.events.LEFTMOUSE,
    'RIGHTMOUSE': bge.events.RIGHTMOUSE,
    'MIDDLEMOUSE': bge.events.MIDDLEMOUSE,
    'WHEELUPMOUSE': bge.events.WHEELUPMOUSE,
    'WHEELDOWNMOUSE': bge.events.WHEELDOWNMOUSE,
}


def load_settings():
    '''Loads the user settings from the config file'''
    SETTINGS['KEYS'] = defs.KEY_MAP


def init(_):
    '''Initializes the controller'''
    LOG.info("Initialized Control")
    load_settings()
    bge.render.showMouse(True)
    bge.logic.scheduler.register_function(update_control)


def update_control():
    '''Updates the control'''
    update_keys()
    update_mouse()


def get_key(key_name):
    '''Returns status of a keyboard key'''
    key_code = bge.events.__dict__[key_name]
    return bge.logic.keyboard.events[key_code]


def update_mouse():
    '''Checks for clickables under the mouse'''
    scene = Common.get_scene(defs.UI_SCENE_NAME)
    cam = scene.active_camera
    pos = list(bge.logic.mouse.position)
    aspect = bge.render.getWindowHeight() / bge.render.getWindowWidth()
    pos[0] = (pos[0] - 0.5) * cam.ortho_scale
    pos[1] = -(pos[1] - 0.5) * cam.ortho_scale * aspect

    raw = cam.rayCast(pos + [-1], pos + [1], 20, "", 0, 0, 2)
    obj = raw[0]
    if obj != update_mouse.__dict__.get('last_active', None):
        old_obj = update_mouse.__dict__.get('last_active', None)
        if old_obj is not None and old_obj in CLICKABLES:
            CLICKABLES[old_obj](None)
    update_mouse.last_active = obj
    if obj is not None and obj in CLICKABLES:
        CLICKABLES[obj](generate_mouse_event(raw))


def generate_mouse_event(raw):
    '''Converts a raycast into a mouse event'''
    obj, pos, nor, poly, uv_pos = raw
    event = dict()
    event['obj'] = obj
    event['pos'] = pos
    event['nor'] = nor
    event['poly'] = poly
    event['uv'] = uv_pos
    for eve in MOUSE_EVENTS:
        event[eve] = bge.logic.mouse.events[MOUSE_EVENTS[eve]]
    return event


def update_keys():
    '''Fires all callbacks associated with keys'''
    for event in EVENT_CALLBACKS:
        if event in bge.logic.keyboard.active_events:
            funct = EVENT_CALLBACKS[event][0]
            args = EVENT_CALLBACKS[event][1]
            if args is None:
                funct()
            else:
                funct(*args)


def add_callback(event, funct, args=None):
    '''Registers a callback for a certain action name'''
    assert event in SETTINGS['KEYS']
    event_id = bge.events.__dict__[SETTINGS['KEYS'][event]]
    EVENT_CALLBACKS[event_id] = (funct, args)


def make_clickable(obj, funct):
    '''Registers a function to be called when the mouse is over an
    object'''
    CLICKABLES[obj] = funct

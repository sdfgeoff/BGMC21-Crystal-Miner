'''Keeps track of what the mouse is doing when over the main display'''
import math

import bge

import defs
import UI
import Control
import Units
import Buildings

selected = None


def set_selected(thing):
    '''Sets the selected objects'''
    global selected
    selected = thing


def get_selected():
    '''Returns the selected object'''
    return selected


def handle(event):
    '''Happens when there is a non-ui event on the main screen'''
    if event is None:
        do_selected(None, None, selected, None)

    else:
        hover, hitpos = get_mouse_over(event)

        if hover is not None:
            pos = bge.logic.map.convert_to_map_coords(hitpos)
            do_selected(event, pos, selected, hover)


def do_selected(event, pos, selec, hover):
    '''Forks the data to whatever is selected'''
    if selec is None:
        # mining(event, pos)
        Units.select_unit(event, pos, selec, hover)
    else:
        if isinstance(selec, UI.BuildingButton):
            Buildings.place_building(event, pos, selec, hover)
            Units.deselect_all_units()
        if selec == Units.assign_target:
            Units.assign_target(event, pos, selec, hover)


def get_mouse_over(event):
    '''Returns the object the mouse is over'''
    main_display = UI.get_main_display()

    cam_pos = event['uv']
    vect = main_display.cam.getScreenVect(cam_pos[0], cam_pos[1])
    ray_to = main_display.cam.worldPosition - vect
    res = main_display.cam.rayCast(ray_to, main_display.cam, defs.WORLD_SIZE)
    hover, hitpos, _ = res
    return hover, hitpos


def mining(event, pos):
    '''Removed the map under the mouse'''
    if event is None:
        return
    if event[defs.MOUSE_MAP['SELECT']] != 0:
        bge.logic.map.mine(math.ceil(pos[0]), math.ceil(pos[1]), 1, True)

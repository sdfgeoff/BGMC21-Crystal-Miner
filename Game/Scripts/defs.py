'''All non-user configurations and definitions'''

import json
import os
import Common
import logging

LOG = logging.getLogger()


EXIT_ON_ERROR = False

# FILES
CONF_FILE = Common.get_from_root('config.cfg')
HIGH_GRAHPICS_FILE = Common.get_from_root('generate.blend')
LOW_GRAHPICS_FILE = Common.get_from_root('generate-low.blend')


# SETTINGS HANDLING
SAVABLE_SETTINGS = ['GRAPHICS', 'SOUNDS', 'START_SIZE']

def load_from_file():
    '''Loads settings from a file'''
    LOG.info("Loading settings from", CONF_FILE)
    if not os.path.isfile(CONF_FILE):
        LOG.info("Settings file does not exist, creating")
        save_file()
    else:
        try:
            new_settings = json.load(open(CONF_FILE))
        except:
            LOG.info("Settings file invalid, recreating")
            save_file()
        for setting in new_settings:
            globals()[setting] = new_settings[setting]
        globals()['WORLD_SIZE'] = START_SIZE_CONVERT_LIST[START_SIZE]

def save_file():
    '''Saves settings to a file'''
    out_dict = dict()
    for setting in SAVABLE_SETTINGS:
        out_dict[setting] = globals()[setting]
    json.dump(out_dict, open(CONF_FILE, 'w'))
    LOG.info("Saving Settings")
    LOG.debug(str(out_dict))

def get_setting(setting):
    return globals()[setting]

def get_setting_name(setting):
    return globals()[setting+'_CHOICES'][get_setting(setting)]

def get_setting_len(setting):
    return len(globals()[setting+'_CHOICES'])


def set_setting(setting, val):
    globals()[setting] = val
    globals()['WORLD_SIZE'] = START_SIZE_CONVERT_LIST[START_SIZE]
    save_file()

# SETTINGS
GRAPHICS = 0
START_SIZE = 0
SOUNDS = 1
START_SIZE_CHOICES = ['TINY', 'NORMAL', 'HUGE']
GRAPHICS_CHOICES = ['HIGH', 'ON']
SOUNDS_CHOICES =['NONE', 'ON']
START_SIZE_CONVERT_LIST = [10, 30, 50]

# Things that should become settings
KEY_MAP = {
    'UP': 'WKEY',
    'DOWN': 'SKEY',
    'LEFT': 'AKEY',
    'RIGHT': 'DKEY',
    'QUITKEY': 'ESCKEY',
}

MOUSE_MAP = {
    'SELECT': 'LEFTMOUSE',
    'MOVE': 'RIGHTMOUSE',
    'VIEW': 'MIDDLEMOUSE',
    'ZOOMIN': 'WHEELUPMOUSE',
    'ZOOMOUT': 'WHEELDOWNMOUSE',
    'MINIMAPVIEW': 'LEFTMOUSE',
    'MINIMAPMOVE': 'RIGHTMOUSE',
}

# PARAMETERS
MIN_ZOOM = 2
MAX_ZOOM = 5
ZOOM_SPEED = 0.2
SCROLL_ROTATE_SPEED = 0.2

MOVE_SPEED = 0.1
MAX_ELEVATION = 0.1  # 0 is the top
MIN_ELEVATION = 1.25
ELEVATION_SENSITIVITY = 0.5
ROTATION_SENSITIVITY = 0.5

# BLENDER ENTITIES
UI_SCENE_NAME = 'UI'
GAME_SCENE_NAME = 'Game'
MAIN_SCREEN_OBJ = 'MainScreen'
IN_GAME_MENU_SCENE = 'MenuScene'
MAIN_SCREEN_CAM = 'GameCam'
MINIMAP_SCREEN_OBJ = 'Minimap'
MINIMAP_SCREEN_CAM = 'MinimapCam'
ECONOMY_DISPLAY = 'EconomyDisplay'
UNIT_SELECTION_OBJECT = 'SelectionIndicator'
DIRECTION_INDICATOR_OBJECT = 'DirectionIndicator'
ENTRY_BUILDING = 'EntryConcretePad'
UNIT_SPAWN_POS = 'EntrySpawnPos'

# QUALITY SETTINGS
NUM_LIGHTS = 5

# BLENDS
LIGHT_BLENDS = ['Models/Lights/Point.blend']
TILE_BLENDS = [
    'Models/UI/Indicators.blend',
    'Models/Map/Tile.blend',
    'Models/Map/Crystal.blend',
    'Models/Buildings/Concrete Pad.blend',
    'Models/Buildings/Light Tower.blend',
    'Models/Buildings/Entry.blend',
    'Models/Units/Miner.blend',
    'Models/Units/Resource Transport.blend',
    'Models/Units/MiningMachine.blend',
]


# MAP CONFIG
WORLD_SIZE = START_SIZE_CONVERT_LIST[START_SIZE]  # Must be less than 255 or the DynTex will explode
BEDROCK_THRESHOLD = 0.2
BEDROCK_SCALE = 0.2
METAL_THRESHOLD = 0.5
NUM_CRYSTAL_ATTEMPTS = 5

# RESOURCES
START_RESOURCES = {
    'Crystal': 0,
    'Ore': 500,
}
MINE_RATE = 5  # per second
ORE_GATHER_RATE = {  # The rate at which metal is gathered per stone hardness
    (0, 1): 0,
    (1, 2): 20,
    (2, 10): 2,
}
CRYSTAL_PER_CRYSTAL = 100
CRYSTAL_MINE_BUFF = 0.2

# UI
BUTTON_LIST = [
    {'type': 'Building', 'name': 'ConcretePad', 'Crystal': 0, 'Ore': 20},
    {'type': 'Building', 'name': 'LightTower', 'Crystal': 0, 'Ore': 150},
    {'type': 'Unit', 'unit_type': 'Mine', 'obj_name': 'Miner', 'level': 0, 'Crystal': 0, 'Ore': 150},
    {'type': 'Unit', 'unit_type': 'Mine', 'obj_name': 'MiningMachine', 'level': 1, 'Crystal': 50, 'Ore': 300},
    {'type': 'Unit', 'unit_type': 'Resource', 'obj_name': 'Resource Truck', 'level':0, 'Crystal': 10, 'Ore': 300},
]


# VEHICLES
MINING_VEHICLE_LEVELS = [
    {'mining_speed': 0.5, 'resource_limit': 100, 'rough_speed': 0.5, 'smooth_speed': 1.0, 'engine_size':1},
    {'mining_speed': 5.0, 'resource_limit': 50, 'rough_speed': 0.4, 'smooth_speed': 0.6, 'engine_size':0.5}
]

RESOURCE_VEHICLE_LEVELS = [
    {'mining_speed': 0.0, 'resource_limit': 500, 'rough_speed': 1.0, 'smooth_speed': 2.0, 'engine_size':0.7}
]

DEFAULT_UNIT = {'mining_speed': 0.0, 'resource_limit': 0, 'rough_speed': 0.5, 'smooth_speed': 1.0}


# SOUNDS
ELEVATOR_SOUNDS = [
    'Sounds/groan.wav',
    'Sounds/creak.wav',
    'Sounds/squeak.wav',
    'Sounds/clank.wav',
    'Sounds/Whirr.wav'
]
ELEVATOR_ANIMATION_FRAMES = 100

WIND_SOUNDS = [  # Sounds that howl....
    'Sounds/Wind1.wav',
    'Sounds/Wind2.wav',
]
AMBIENT_SOUNDS = [  # Sounds that play at various periodicities
    # ('file/name.wav, probability, volume)
    ('Sounds/ambient1.wav', 0.005, 2)
]

ENGINE_SOUND = 'Sounds/bigengine.wav'
LIGHT_SOUND = 'Sounds/generator.wav'

PRELOAD_SOUND_LIST = ELEVATOR_SOUNDS + WIND_SOUNDS + [ENGINE_SOUND] + [LIGHT_SOUND]

load_from_file()

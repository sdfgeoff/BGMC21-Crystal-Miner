'''Handles all UI operations'''

import bge
import mathutils
import logging

import Common
import Control
import defs
import Loader
import Economy
import Mouse
import Buildings
import Units

LOG = logging.getLogger()


def add_ui_scene(_):
    '''Adds the UI scene'''
    Loader.display_load_message("Loading UI")
    LOG.debug("Adding UI Scene")
    bge.logic.addScene(defs.UI_SCENE_NAME)


def configure_viewports(_):
    '''Creates the viewports used to display the main scene'''
    MainDisplay()
    MiniMap()
    Loader.display_load_message("Loaded UI")
    Control.add_callback('QUITKEY', on_escape_key)


def on_escape_key():
    if Common.get_scene(defs.IN_GAME_MENU_SCENE) == None:
        if Control.get_key('ESCKEY') == 1:
            selected = Mouse.get_selected()
            if selected is not None:
                Mouse.do_selected(None, None, selected, None)
                Mouse.set_selected(None)
                Units.deselect_all_units()
            else:
                bge.logic.addScene(defs.IN_GAME_MENU_SCENE)

def configure_interface(_):
    '''Configures the non-display elements of the UI such as buttons'''
    ui_scene = Common.get_scene(defs.UI_SCENE_NAME)
    assert isinstance(ui_scene, bge.types.KX_Scene)
    text_obj = ui_scene.objects[defs.ECONOMY_DISPLAY]
    Economy.get_wallet().set_text_object(text_obj)
    Economy.get_wallet().add_resource(defs.START_RESOURCES)

    for id_num, button in enumerate(defs.BUTTON_LIST):
        button_obj = ui_scene.objects['Button' + str(id_num)]
        if button['type'] == 'Building':
            BuildingButton(button_obj, button)
        if button['type'] == 'Unit':
            UnitButton(button_obj, button)


def get_main_display():
    '''Returns the instance of the main display'''
    ui_scene = Common.get_scene(defs.UI_SCENE_NAME)
    assert isinstance(ui_scene, bge.types.KX_Scene)
    for obj in ui_scene.objects:
        if 'screen' in obj:
            if isinstance(obj['screen'], MainDisplay):
                return obj['screen']


def get_minimap_display():
    '''Returns the instance of the main display'''
    ui_scene = Common.get_scene(defs.UI_SCENE_NAME)
    assert isinstance(ui_scene, bge.types.KX_Scene)
    for obj in ui_scene.objects:
        if isinstance(obj['screen'], MiniMap):
            return obj['screen']


class Display(object):
    '''A generic display object, using a camera and a plane to set
    it's size and location'''
    coords = [None, None, None, None]

    def __init__(self, cam, screen_obj):
        LOG.debug("Creating display from %s and %s", cam, screen_obj)
        self.cam = cam
        self.obj = screen_obj
        self.obj['screen'] = self
        self.recalc_size()
        # LOG.info("Created display from %s and %s", cam, screen_obj)

    def recalc_size(self):
        '''Updates the size of the display on-screen
        only works assuming display camera is z down, center of world'''
        min_dim, max_dim = self._get_world_dim()

        screen_min = self._ui_world_to_screen(min_dim)
        screen_max = self._ui_world_to_screen(max_dim)

        self.coords = [
            int(screen_min.x),
            int(screen_min.y),
            int(screen_max.x),
            int(screen_max.y),
        ]
        # LOG.debug("Display Coords are [%d, %d, %d, %d]", *self.coords)

        self.cam.setViewport(*self.coords)
        self.cam.useViewport = True

    def _get_world_dim(self):
        '''Returns the world dimensions of the object'''
        material = self.obj.meshes[0].materials[0]
        mat_id = bge.texture.materialID(self.obj, str(material))
        max_dim = mathutils.Vector([-9999, -9999, -9999])
        min_dim = mathutils.Vector([9999, 9999, 9999])
        for vert_id in range(self.obj.meshes[0].getVertexArrayLength(mat_id)):
            local_pos = self.obj.meshes[0].getVertex(mat_id, vert_id).XYZ
            world_pos = self.obj.worldTransform * local_pos
            max_dim.x = max(max_dim.x, world_pos.x)
            max_dim.y = max(max_dim.y, world_pos.y)
            max_dim.z = max(max_dim.z, world_pos.z)
            min_dim.x = min(min_dim.x, world_pos.x)
            min_dim.y = min(min_dim.y, world_pos.y)
            min_dim.z = min(min_dim.z, world_pos.z)
        return min_dim, max_dim

    def _ui_world_to_screen(self, vec):
        '''Converts a UI world space into UI screen space'''
        width = bge.render.getWindowWidth()
        height = bge.render.getWindowHeight()
        asp = width / height
        ortho = self.obj.scene.active_camera.ortho_scale
        screen_transform = mathutils.Matrix.Translation([0.5, 0.5 / asp, 0])

        vec = vec / ortho
        vec = screen_transform * vec
        vec.x *= width
        vec.y *= height * asp
        return vec

    def get_coords(self):
        '''Returns the screen-space corners of the display'''
        return self.coords


class MiniMap(Display):
    '''The minimap in the top left of the screen'''
    def __init__(self):
        main_scene = Common.get_scene(defs.GAME_SCENE_NAME)
        ui_scene = Common.get_scene(defs.UI_SCENE_NAME)
        assert isinstance(main_scene, bge.types.KX_Scene)
        assert isinstance(ui_scene, bge.types.KX_Scene)

        map_cam = main_scene.objects[defs.MINIMAP_SCREEN_CAM]
        map_display = ui_scene.objects[defs.MINIMAP_SCREEN_OBJ]
        super().__init__(map_cam, map_display)
        map_cam.ortho_scale = defs.WORLD_SIZE
        Control.make_clickable(self.obj, self.mouse_event)

    def mouse_event(self, event):
        '''Handles any click events on the minimap'''
        if event is None:
            return
        if event[defs.MOUSE_MAP['MINIMAPVIEW']] != 0:
            pos = event['uv']
            world_pos = (pos - mathutils.Vector([0.5, 0.5])) * defs.WORLD_SIZE
            get_main_display().set_position(world_pos)

            if event[defs.MOUSE_MAP['ZOOMIN']] != 0:
                get_main_display().orbit(0, defs.SCROLL_ROTATE_SPEED)
            if event[defs.MOUSE_MAP['ZOOMOUT']] != 0:
                get_main_display().orbit(0, -defs.SCROLL_ROTATE_SPEED)


class MainDisplay(Display):
    '''The main viewport onto the game area'''
    def __init__(self):
        main_scene = Common.get_scene(defs.GAME_SCENE_NAME)
        ui_scene = Common.get_scene(defs.UI_SCENE_NAME)
        assert isinstance(main_scene, bge.types.KX_Scene)
        assert isinstance(ui_scene, bge.types.KX_Scene)

        map_cam = main_scene.objects[defs.MAIN_SCREEN_CAM]
        map_display = ui_scene.objects[defs.MAIN_SCREEN_OBJ]
        super().__init__(map_cam, map_display)
        self.zoom(0)

        self.mouse_pos = None
        self._register_controls()

    def _register_controls(self):
        '''Registers the interactions with the mainscreen with the
        control module'''
        Control.add_callback('UP', self.translate, args=[0, 1])
        Control.add_callback('DOWN', self.translate, args=[0, -1])
        Control.add_callback('LEFT', self.translate, args=[-1, 0])
        Control.add_callback('RIGHT', self.translate, args=[1, 0])
        Control.make_clickable(self.obj, self.mouse_event)

    def set_position(self, pos):
        '''Sets the position of the camera directly'''
        self.cam.parent.parent.worldPosition.xy = pos

    def translate(self, delta_x, delta_y):
        '''Translates the view point'''
        vec = mathutils.Vector([delta_x, 0, -delta_y])
        vec *= defs.MOVE_SPEED
        vec = self.cam.parent.parent.getAxisVect(vec)
        pos = self.cam.parent.parent.worldPosition + vec
        move_limit = defs.WORLD_SIZE / 2
        pos.x = Common.constrain(pos.x, -move_limit, move_limit)
        pos.y = Common.constrain(pos.y, -move_limit, move_limit)
        self.cam.parent.parent.worldPosition = pos

    def orbit(self, elevation, rotation):
        '''Sets the elevation and angle of the camera'''
        rotation *= defs.ROTATION_SENSITIVITY
        elevation *= defs.ELEVATION_SENSITIVITY
        ele = self.cam.parent.worldOrientation.to_euler()
        ele.y -= elevation
        ele.y = Common.constrain(ele.y, defs.MAX_ELEVATION, defs.MIN_ELEVATION)
        self.cam.parent.parent.applyRotation([0, 0, rotation])
        self.cam.parent.worldOrientation = ele

    def zoom(self, zoom):
        '''Sets the zoom level of the main camera'''
        new_zoom = self.cam.localPosition.y - zoom
        new_zoom = Common.constrain(new_zoom, -defs.MAX_ZOOM, -defs.MIN_ZOOM)
        self.cam.localPosition.y = new_zoom

    def mouse_event(self, event):
        '''Runs whenever the mouse is over the display with details of
        the events. Handles things such as rotation'''
        if event is None:
            self.mouse_pos = None
            Mouse.handle(None)
            return
        if event[defs.MOUSE_MAP['VIEW']] != 0:
            if self.mouse_pos is None:
                self.mouse_pos = mathutils.Vector(event['pos'])
            else:
                current_pos = mathutils.Vector(event['pos'])
                delta = self.mouse_pos - current_pos
                self.mouse_pos = current_pos
                self.orbit(delta.y, delta.x)
        else:
            self.mouse_pos = None
            Mouse.handle(event)

        if event[defs.MOUSE_MAP['ZOOMIN']] != 0:
            self.zoom(-defs.ZOOM_SPEED)
        if event[defs.MOUSE_MAP['ZOOMOUT']] != 0:
            self.zoom(defs.ZOOM_SPEED)


class BuildingButton(object):
    '''Makes a game object clickable to purchase a building'''
    def __init__(self, obj, data):
        self.obj = obj
        self.data = data
        self.update_text()
        Control.make_clickable(self.obj, self.mouse_event)
        self.obj.color = [0.5, 0.5, 0.5, 1]

    def update_text(self):
        '''Displays the cost of the item'''
        text_obj = self.obj.children[0]
        text_obj.resolution = 3
        text_obj.text = "O:{:3d} C:{:3d}".format(self.data['Ore'],
                                                 self.data['Crystal'])

    def mouse_event(self, event):
        '''What happens when you click on the button'''
        if event is None:
            self.obj.color = [0.5, 0.5, 0.5, 1]
            return
        self.obj.color = [1, 1, 1, 1]
        if event['LEFTMOUSE'] == bge.logic.KX_SENSOR_JUST_ACTIVATED:
            Mouse.set_selected(self)


class UnitButton(object):
    '''Makes a game object clickable to spawn a unit'''
    def __init__(self, obj, data):
        self.obj = obj
        self.data = data
        self.update_text()
        Control.make_clickable(self.obj, self.mouse_event)
        self.obj.color = [0.5, 0.5, 0.5, 1]

    def update_text(self):
        '''Displays the cost of the item'''
        text_obj = self.obj.children[0]
        text_obj.resolution = 3
        text_obj.text = "O:{:3d} C:{:3d}".format(self.data['Ore'],
                                                 self.data['Crystal'])

    def mouse_event(self, event):
        '''What happens when you click on the button'''
        if event is None:
            self.obj.color = [0.5, 0.5, 0.5, 1]
            return
        self.obj.color = [1, 1, 1, 1]
        if event['LEFTMOUSE'] == bge.logic.KX_SENSOR_JUST_ACTIVATED:
            entry = Buildings.get_entry()
            cost = {'Ore': self.data['Ore'],
                'Crystal': self.data['Crystal']}
            entry.spawn_object(self.data, cost)


def fix_text_res(cont):
    '''Increases text resolution'''
    cont.owner.resolution = 5

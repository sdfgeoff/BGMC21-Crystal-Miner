'''Any building operations'''
import bge
import logging
import mathutils
import math

import UI
import Units
import Economy
import defs
import Mouse
import Control
import Sounds

LOG = logging.getLogger()


def get_entry():
    '''Returns the entry'''
    main_scene = bge.logic.getCurrentScene()
    return [o for o in main_scene.objects if isinstance(o, EntryBuilding)][0]


def place_entry(pos):
    '''Places an entry object at the specified location'''
    scene = bge.logic.getCurrentScene()
    entry = scene.addObject(defs.ENTRY_BUILDING, scene.objects[0])
    EntryBuilding(entry, pos)


class EntryBuilding(bge.types.KX_GameObject):
    '''Makes an entry building from a given object'''
    def __init__(self, _, pos):
        LOG.debug("Made Entry")
        self.pos = pos
        self.worldScale = [1, 1, 1]
        self.worldPosition = pos
        self.worldPosition.x -= 0.5
        self.worldPosition.y -= 0.5
        self.worldPosition.z = -0.1
        self.worldOrientation = [0, 0, 0]
        self['Droppable'] = True
        self['SmoothSurface'] = 1.0
        self.spawn_object(defs.BUTTON_LIST[2], {})

        elevator_duration = defs.ELEVATOR_ANIMATION_FRAMES / 60
        self.sound = Sounds.ElevatorSound(self, elevator_duration)

        node = bge.logic.map.get_closest_node(self.worldPosition)
        node_x, node_y = node.mapPosition
        for x_pos in range(node_x, node_x + 2):
            for y_pos in range(node_y, node_y + 2):
                bge.logic.map.get_node([x_pos, y_pos]).add_object(self)

            bge.logic.map.get_node([x_pos, node_y]).add_object({'Blocks':True})
        self.map_pos = [node_x, node_y]

        UI.get_main_display().set_position(self.worldPosition.xy)
        self.play_animation(-1)

    def get_map_position(self):
        '''Returns the position of the building on the map'''
        return self.map_pos

    def get_drop_position(self):
        '''Returns the position of the building on the map'''
        return mathutils.Vector([self.map_pos[0], self.map_pos[1] + 0.5])

    def play_animation(self, direction):
        '''Animates the elevator going up and down'''
        self.sound.play(direction)
        if direction == 1:
            start = 0
            stop = defs.ELEVATOR_ANIMATION_FRAMES
            speed = 1
        elif direction == -1:
            start = defs.ELEVATOR_ANIMATION_FRAMES
            stop = 0
            speed = 1
        for child in self.childrenRecursive:
            if 'Animate' in child:
                action_name = child.name
                child.playAction(action_name, start, stop, speed=speed)


    def spawn_object(self, obj_data, cost):
        '''Spawns an object at the mine entry'''
        if Economy.get_wallet().withdraw(cost):
            spawn_pos = self.children[defs.UNIT_SPAWN_POS]
            Units.add_unit(obj_data, spawn_pos)

    def add_resources(self, resource_dict):
        '''Adds resources to the global pool'''
        Economy.get_wallet().add_resource(resource_dict)


def place_building(event, pos, sel, _):
    '''Places a building at the specified location'''
    phantom = place_building.__dict__.get('phantom', None)

    if event is None:
        # Mouse moved off screen or no longer active selected
        if phantom is not None:
            phantom.endObject()
            place_building.phantom = None
        return
    pos[0] = math.ceil(pos[0])
    pos[1] = math.ceil(pos[1])
    node = bge.logic.map.get_node(pos)
    terrain = node.get_terrain()
    placables = node.get_contains()
    if terrain > 0 or len(placables) > 0:
        # Terrain already occupied or not suitable for building
        if phantom is not None:
            phantom.endObject()
            place_building.phantom = None
        return

    if phantom is None:
        # Add the phantom
        scene = bge.logic.getCurrentScene()
        ref = scene.objects[0]
        obj = scene.objectsInactive[sel.data['name']]
        place_building.phantom = scene.addObject(obj, ref)
        place_building.phantom.worldPosition = node.worldPosition
        phantom = place_building.phantom
    elif phantom.name == sel.data['name']:
        phantom.worldPosition = node.worldPosition

    if event['LEFTMOUSE'] == bge.logic.KX_SENSOR_JUST_ACTIVATED:
        # Make the phantom object real
        cost = {'Ore': sel.data['Ore'],
                'Crystal': sel.data['Crystal']}
        if Economy.get_wallet().withdraw(cost):  # Check cost
            node.add_object(place_building.phantom)
            if sel.data['name'] == 'LightTower':
                Sounds.LightSound(place_building.phantom)
            place_building.phantom = None
            if Control.get_key('LEFTSHIFTKEY') == 0:
                Mouse.set_selected(None)

    if event['RIGHTMOUSE'] == bge.logic.KX_SENSOR_JUST_ACTIVATED:
        # Cancel with right mouse click
        Mouse.set_selected(None)
        phantom.endObject()
        place_building.phantom = None

import logging

import bge
import aud

import Common
import Loader
import defs

LOG = logging.getLogger()


DEVICE = aud.device()

WIND_DAMPING = 0.01        # How much the wind returns to normal
WIND_CHANGE_SPEED = 0.1    # How fast the wind speed can change
WIND_MIN = 2               # 'Normal' wind level
WIND_MAX = 0.2             # Maximum, used to constrain pitch
WIND_VOLUME = 0.2            # Volume multiplier

def init_sounds(cont):
    if defs.SOUNDS:
        Loader.display_load_message("Loading Sounds")
        for sound in defs.PRELOAD_SOUND_LIST:
            create_factory(sound)
        for sound in defs.WIND_SOUNDS:
            WindEffect(sound)
        for ambient in defs.AMBIENT_SOUNDS:
            sound = ambient[0]
            probability = ambient[1]
            volume = ambient[2]
            RandomEffect(sound, probability, volume)
    else:
        Loader.display_load_message("Sounds Disabled")


class WindEffect(object):
    '''Creates a randomly varying wind effect from given file name'''
    def __init__(self, path):
        bge.logic.scheduler.register_function(self.update)
        self.wind_fac = create_factory(path).loop(-1)
        self.wind = DEVICE.play(self.wind_fac)
        self.wind.volume = 0.0
        #cont.activate(self.wind1)
        self.wind_level = WIND_MIN
        #cont.activate(self.wind1)

    def update(self):
        '''Varies the wind levels in a howly sort of way'''
        perturb = self.wind_level + (bge.logic.getRandomFloat() - 0.5) * WIND_CHANGE_SPEED
        self.wind_level = perturb * (1 - WIND_DAMPING) + WIND_MIN * WIND_DAMPING
        self.wind_level = max(self.wind_level, WIND_MAX)
        self.wind.pitch = 1/(self.wind_level)
        self.wind.volume = (1/(self.wind_level)) * WIND_VOLUME


class RandomEffect(object):
    '''Plays a sound at probability based intervals'''
    def __init__(self, path, probability, volume):
        bge.logic.scheduler.register_function(self.update)
        self.fac = create_factory(path)
        self.prob = probability
        self.volume = volume

    def update(self):
        if bge.logic.getRandomFloat() < self.prob:
            handle = DEVICE.play(self.fac)
            handle.volume = self.volume


class ElevatorSound(object):
    '''a sound for an elevator going up and down'''
    def __init__(self, obj, duration):
        if not defs.SOUNDS:
            return
        self.obj = obj
        self.duration = duration
        self.sounds = dict()
        for sound in defs.ELEVATOR_SOUNDS:
            sound_name = sound.split('/')[-1].split('.')[0]
            self.sounds[sound_name] = create_factory(sound)

    def play(self, direction):
        if direction == 1:
            whirr = DEVICE.play(self.sounds['Whirr'])
            whirr.volume = 2
        pass


class EngineSound(object):
    def __init__(self, obj):
        if not defs.SOUNDS:
            return
        bge.logic.scheduler.register_function(self.update)
        self.sound = DEVICE.play(create_factory(defs.ENGINE_SOUND).loop(-1))
        self.sound.volume = 0.0
        self.obj = obj

    def update(self):
        engine_size = self.obj.specs['engine_size']
        level = self.obj.worldLinearVelocity.length + 0.3
        dist = self.obj.getDistanceTo(self.obj.scene.active_camera)
        if dist < 100:
            self.sound.volume = max(0, level+0.3-dist/10) / engine_size
            self.sound.pitch = (level+0.5) * engine_size
        else:
            self.sound.volume = 0.0
        return


class LightSound(object):
    def __init__(self, obj):
        if not defs.SOUNDS:
            return
        bge.logic.scheduler.register_function(self.update)
        self.sound = DEVICE.play(create_factory(defs.LIGHT_SOUND).loop(-1))
        self.sound.volume = 1.0
        self.obj = obj

    def update(self):
        dist = self.obj.getDistanceTo(self.obj.scene.active_camera)
        self.sound.volume = max(0, 1-dist/10)
        return


def create_factory(path):
    create_factory.known = create_factory.__dict__.get('known', dict())
    if path not in create_factory.known:
        full_path = Common.get_from_root(path)
        fac = aud.Factory(full_path)
        fac.buffer()
        create_factory.known[path] = fac
        LOG.debug("Loading Sound %s", full_path)
    else:
        fac = create_factory.known[path]
    return fac

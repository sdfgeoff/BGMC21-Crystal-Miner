'''This file contains the class that performs A* pathfinding'''
import time
try:
    import Map
except ImportError:
    class Map:
        '''"Module" to allow testing independantly from the game'''
        class GlobalMap:
            '''Fke map class'''

ITERATION_LIMIT = 100

class NavigationNode(object):
    ''' A point on the map, consisting of what it is and where, and more
        importantly, it's f, g, and h values (for pathfinding).

    A note of caution:
        When comparing nodes, they behave differently with different methods.
        If two nodes are being tested for >, >=, <, <= (__gt__, __ge__, __lt__,
        __le__), then the f value is compared. This is so that they can be
        sorted easily.
        If two nodes are being compared be the == operator (__eq__), then it is
        their position, pos, being compared. This way, sets don't duplicate
        different nodes on the same spot.
    '''

    def __init__(self, pos, cost_to=0, heuristic_cost=0, parent=None):
        '''
        Inputs:
            pos - the position of the node
            g - the distance from the initial node of interest
            h - heuristic guess at how far it is from the final node. Set to
                0 for dijksra's algorithm, or input the position of the final
                node to calculate the h value
            parent - setting the node that this one was calculated from

        The final value f is simply g+h, is what is usually compared.
        '''
        self.pos = tuple(pos)
        self.cost_to = cost_to
        self.heuristic_cost = heuristic_cost
        self.total_cost = self.cost_to + self.heuristic_cost
        if isinstance(parent, NavigationNode):
            self.parent = parent
        else:
            self.parent = None

    def get_pos(self):
        '''Returns the position of this node'''
        return self.pos

    def get_distance_to(self, pos):
        ''' Calculates the 2D distance between two points on the map'''
        delta_x = pos[0] - self.pos[0]
        delta_y = pos[1] - self.pos[1]
        return max(abs(delta_x), abs(delta_y))

    def __repr__(self):
        ''' The representation of the node is 'NavigationNode(pos, f)'''
        return 'NavigationNode({0}, {1})'.format(self.pos, self.heuristic_cost)

    def __str__(self):
        return str(self.pos)

    def __eq__(self, other):
        ''' NavigationNode.__eq__(other) <==> NavigationNode==other
            Checks if this node is at the same position as all others
            Note: this is different from all other equalities!
        '''
        if not isinstance(other, NavigationNode):
            return self.pos == other
        return self.pos == other.pos

    def __ge__(self, other):
        ''' NavigationNode.__ge__(other) <==> NavigationNode>=other
            Can compare total_cost value with other nodes and numbers
        '''
        if isinstance(other, NavigationNode):
            return self.total_cost >= other
        return self.total_cost >= other.total_cost

    def __gt__(self, other):
        ''' NavigationNode.__gt__(other) <==> NavigationNode>other
            Can compare total_cost value with other nodes and numbers
        '''
        if not isinstance(other, NavigationNode):
            return self.total_cost > other
        return self.total_cost > other.total_cost

    def __le__(self, other):
        ''' NavigationNode.__le__(other) <==> NavigationNode<=other
            Can compare total_cost value with other nodes and numbers
        '''
        if not isinstance(other, NavigationNode):
            return self.total_cost <= other
        return self.total_cost <= other.total_cost

    def __lt__(self, other):
        ''' NavigationNode.__lt__(other) <==> NavigationNode<other
            Can compare total_cost value with other nodes and numbers
        '''
        if not isinstance(other, NavigationNode):
            return self.total_cost < other
        return self.total_cost < other.total_cost

    def __ne__(self, other):
        ''' NavigationNode.__ne__(other) <==> NavigationNode!=other
            Can compare total_cost value with other nodes and numbers
        '''
        if not isinstance(other, NavigationNode):
            return self.total_cost != other
        return self.total_cost != other.total_cost

    def __hash__(self):
        ''' Ensures no two nodes of the same position occur in a set'''
        return hash(self.pos)

    def __getitem__(self, key):
        ''' Makes it easy to get the position'''
        return self.pos[key]


class Astar(object):
    ''' Contains pathfinding algorithm based on a unit specific speed map'''

    def __init__(self, speed_map=None, map_transpose=True):
        self.speed_map = speed_map
        if isinstance(self.speed_map, Map.GlobalMap):
            self.map_size = (self.speed_map.size, self.speed_map.size)
        else:
            self.map_size = (len(speed_map[0]), len(speed_map))
        self.map_transpose = map_transpose
        self.unit = None

    def set_map(self, speed_map):
        ''' Given a map of how fast a unit travels across parts of it, this
            function stores it for the pathfinding algorithm'''
        self.speed_map = speed_map
        if isinstance(self.speed_map, Map.GlobalMap):
            self.map_size = (self.speed_map.size, self.speed_map.size)
        else:
            self.map_size = (len(speed_map[0]), len(speed_map))

    def get_speed_map_point(self, pos, unit):
        '''Returns the speed on the speed map at a given point'''
        if isinstance(self.speed_map, Map.GlobalMap):
            return self.speed_map.get_node(pos).get_speed(unit)
        else:
            if self.map_transpose:
                return self.speed_map[pos[1]][pos[0]]
            else:
                return self.speed_map[pos[0]][pos[1]]

    def __str__(self):
        out_str = ''
        for y_pos in range(self.map_size[1]):
            for x_pos in range(self.map_size[0]):
                out_str += str(self.get_speed_map_point([x_pos, y_pos], self.unit))
                out_str += ' '
            out_str += '\n'
        return out_str

    def print_path(self, node_path):
        '''Displays the path picked in a human-readable fashion'''
        out_str = ''
        for y_pos in range(self.map_size[1]):
            for x_pos in range(self.map_size[0]):
                found = False
                if self.get_speed_map_point([x_pos, y_pos], self.unit) < 0.001:
                    out_str += '[]'
                else:
                    for node in node_path:
                        if isinstance(node, NavigationNode):
                            pos = node.get_pos()
                        else:
                            pos = node
                        if pos[0] == x_pos and pos[1] == y_pos:
                            out_str += '><'
                            found = True
                    if not found:
                        out_str += '  '
            out_str += '\n'
        print(out_str)

    def find_surrounding_nodes(self, initial_node, unit):
        ''' Returns the set of nodes surrounding the given ones'''
        pos = initial_node.pos

        surrounding_nodes = set()
        cart_positions = ((pos[0], pos[1] + 1),
                          (pos[0] + 1, pos[1]),
                          (pos[0], pos[1] - 1),
                          (pos[0] - 1, pos[1]))
        diag_positions = ((pos[0] + 1, pos[1] - 1),
                          (pos[0] + 1, pos[1] + 1),
                          (pos[0] - 1, pos[1] + 1),
                          (pos[0] - 1, pos[1] - 1))

        for new_pos in cart_positions:
            if self.pos_in_map(new_pos):
                node_speed = self.get_speed_map_point(new_pos, unit)
                # Can travel on this node
                if node_speed > 0:
                    cost_to = initial_node.cost_to + 1 / node_speed
                    surrounding_nodes.add(
                        NavigationNode(new_pos, cost_to))
        for new_pos in diag_positions:
            if self.pos_in_map(new_pos):
                node_speed = self.get_speed_map_point(new_pos, unit)
                if node_speed > 0:
                    cost_to = initial_node.cost_to + 2 / node_speed
                    surrounding_nodes.add(
                        NavigationNode(new_pos, cost_to))
        return surrounding_nodes

    def pos_in_map(self, pos):
        '''Returns True if a position is in the map'''
        return pos[0] >= 0 and \
            pos[0] < self.map_size[0] and \
            pos[1] >= 0 and \
            pos[1] < self.map_size[1]

    def navigate_between(self, initial_coord, final_coord, unit=None):
        ''' If a path between the nodes can be found, this will find
            on of the fastest routes using Astar
        '''
        if self.get_speed_map_point(final_coord, unit) == 0.0:
            return None  # Dumb check to see if target is reachable

        self.unit = unit

        open_nodes = set()    # Nodes to search for routes
        closed_nodes = set()  # Nodes that have been searched for routes
        surr_nodes = set()    # Nodes that surround the node of interest
        node_path = []
        initial_node = NavigationNode(tuple(initial_coord), 0, 0)
        initial_node.heuristic_cost = initial_node.get_distance_to(tuple(final_coord))
        open_nodes.add(initial_node)
        found_end = False
        iterations = 0


        start_time = time.time()
        while found_end is False and len(open_nodes) != 0:  # Getting there
            iterations += 1
            if iterations > ITERATION_LIMIT:
                return None
            parent_node = min(open_nodes)
            surr_nodes = self.find_surrounding_nodes(parent_node, unit)
            for surr_node in surr_nodes:
                if surr_node not in closed_nodes:
                    new_h = surr_node.get_distance_to(tuple(final_coord))
                    new = NavigationNode(surr_node.pos, surr_node.cost_to,
                                         new_h, parent_node)
                    open_nodes.add(new)
                if surr_node.pos == tuple(final_coord):
                    node_path.append(new)
                    found_end = True
            closed_nodes.add(parent_node)
            open_nodes.remove(parent_node)
        if len(node_path) != 0:
            next_on_path = node_path[0]
            while next_on_path is not None:  # Getting back
                next_on_path = next_on_path.parent
                if isinstance(next_on_path, NavigationNode):
                    node_path.append(next_on_path)
        else:
            return None
        #print(time.time() - start_time, iterations)
        return [node.pos for node in node_path]


def test1():
    '''Performs a test on the algorithm'''
    #            0    1    2    3    4    5    6    7    8    9
    terrain = [[1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 0.0, 1.0],  # 0
               [1.0, 1.0, 1.0, 9.0, 1.0, 1.0, 1.0, 1.0, 0.0, 1.0],  # 1
               [1.0, 1.0, 1.0, 9.0, 1.0, 1.0, 1.0, 1.0, 0.0, 1.0],  # 2
               [1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 0.0, 1.0],  # 3
               [1.0, 1.0, 1.0, 9.0, 1.0, 1.0, 1.0, 1.0, 0.0, 1.0],  # 4
               [1.0, 1.0, 1.0, 9.0, 1.0, 1.0, 1.0, 1.0, 0.0, 1.0],  # 5
               [1.0, 1.0, 1.0, 9.0, 1.0, 1.0, 1.0, 1.0, 0.0, 1.0],  # 6
               [1.0, 1.0, 1.0, 9.0, 1.0, 1.0, 1.0, 1.0, 0.0, 1.0],  # 7
               [1.0, 1.0, 1.0, 9.0, 1.0, 1.0, 0.0, 0.0, 0.0, 1.0],  # 8
               [1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0]]  # 9

    start = [0, 0]
    finish = [9, 4]

    test_map = Astar(terrain, map_transpose=True)
    print(test_map)
    path = test_map.navigate_between(start, finish)
    test_map.print_path(path)
    print(path)

if __name__ == "__main__":
    test1()

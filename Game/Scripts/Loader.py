'''Handles the loading and startup of the game'''

import bge
import logging
import traceback

import defs
import Common
import Map
import UI
import Control
import Lights
import Units
import Sounds

LOG = logging.getLogger()


def load_next(cont):
    '''Handles the loading state machine'''
    load_state = cont.owner.get('load_state', 0)
    if load_state < len(LOAD_MAP):
        try:
            res = LOAD_MAP[load_state](cont)
            if res is not False:
                # Move on to next state
                cont.owner['load_state'] = load_state + 1
        except Exception:
            LOG.error(traceback.format_exc())
            cont.owner['load_state'] = load_state + 1
            if defs.EXIT_ON_ERROR:
                bge.logic.endGame()

    else:
        return True


def load_lights(cont):
    '''Libloads in lights. This must be done after all the models are
    loaded'''
    for _ in range(defs.NUM_LIGHTS):
        for blend in defs.LIGHT_BLENDS:
            blend_path = Common.get_from_root(blend)
            lamp = add_unique(blend_path)[0]
            added_lamp = cont.owner.scene.addObject(lamp, cont.owner)
            added_lamp.energy = 0.0
    display_load_message("Loaded Lights")


def load_tiles(_):
    '''Libloads in the tiles required for the map'''
    count = load_tiles.__dict__.get('count', 0)
    total = len(defs.TILE_BLENDS)

    blend = defs.TILE_BLENDS[count]
    blend_path = Common.get_from_root(blend)
    add_from_blend(blend_path)
    display_load_message("Loading Objects {}/{}".format(count, total))

    load_tiles.count = count + 1
    if count < total - 1:
        return False
    else:
        return True


def display_load_message(text):
    '''Updates the loading screen'''
    text_objs = bge.logic.getCurrentScene().active_camera.children
    text_objs['smalltext'].text = text
    text_objs['smalltext'].resolution = 5
    state = bge.logic.getCurrentController().owner['load_state']
    total = len(LOAD_MAP)
    percent = int((state / total) * 100)

    text_objs['largetext'].text = "Loading {}%".format(percent)
    text_objs['largetext'].resolution = 5
    LOG.info(text)


def add_unique(file_name):
    '''Caution, does not work with non-packed textures'''
    blend_bytes = open(file_name, 'rb').read()
    identifier = file_name + str(0)
    while identifier in bge.logic.LibList():
        identifier = identifier + str(int(identifier[-1]) + 1)
    scene = bge.logic.getCurrentScene()
    for obj in scene.objectsInactive:
        obj['ALREADY_KNWON'] = 0

    LOG.debug("Loading blend from bytes: %s", file_name)
    bge.logic.LibLoad(identifier, 'Scene', blend_bytes)

    new_objs = [o for o in scene.objectsInactive if 'ALREADY_KNWON' not in o]
    for obj in new_objs:
        obj['ALREADY_KNWON'] = 0
    return new_objs


def add_from_blend(file_name):
    '''Returns a list of objects loaded from the blend'''
    scene = bge.logic.getCurrentScene()
    for obj in scene.objectsInactive:
        obj['ALREADY_KNWON'] = 0

    LOG.debug("Loading blend : %s", file_name)
    bge.logic.LibLoad(file_name, 'Scene', load_actions=True)
    new_objs = [o for o in scene.objectsInactive if 'ALREADY_KNWON' not in o]
    for obj in new_objs:
        obj['ALREADY_KNWON'] = 0
    return new_objs


def end_loader(cont):
    '''Hides the loading text'''
    text_objs = cont.owner.scene.active_camera.children
    for obj in text_objs:
        obj.text = ''


def wait(_):
    '''Use to insert a frame's break in the load map'''
    return


LOAD_MAP = [
    Control.init,
    load_lights,
    Lights.init,
    load_tiles,
    Sounds.init_sounds,
    UI.add_ui_scene,
    UI.configure_viewports,
    UI.configure_interface,
    Units.init_units,
    Map.generate_map,
    end_loader,
]

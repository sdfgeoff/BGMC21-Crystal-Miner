'''Hanldes the non-game interface, ie the main menu'''
import logging

import bge

import defs
import Common


LOG = logging.getLogger()


def button(cont):
    '''Makes the object act like a button'''
    for sens in cont.sensors:
        if not sens.positive:
            return

    increment_setting(cont.owner['Button'], cont.owner['Delta'])
    regenerate_options_string(cont)



def regenerate_options_string(cont):
    '''Recreates the text object used to display the current settings'''
    settings_display_dict = {
        'graphics': defs.get_setting_name('GRAPHICS'),
        'sounds': defs.get_setting_name('SOUNDS'),
        'size': defs.get_setting_name('START_SIZE'),

    }
    cont.owner.parent.text = \
'''Graphics        < {graphics:^6} >
Sounds          < {sounds:^6} >
Start Size      < {size:^6} >'''.format(**settings_display_dict)


def increment_setting(setting, delta):
    '''Changes a setting'''
    new_val = defs.get_setting(setting) + delta
    new_val = Common.wrap(new_val, 0, defs.get_setting_len(setting) - 1)
    defs.set_setting(setting, new_val)


def start_game(cont):
    '''Starts the game'''
    for sens in cont.sensors:
        if not sens.positive:
            return
    if defs.get_setting_name('GRAPHICS') == 'HIGH':
        LOG.info("Starting High graphics")
        blend = defs.HIGH_GRAHPICS_FILE
    else:
        LOG.info("Starting Low Graphics")
        blend = defs.LOW_GRAHPICS_FILE
    bge.logic.startGame(blend)

def fix_text_res(cont):
    '''Increases text resolution'''
    cont.owner.resolution = 5

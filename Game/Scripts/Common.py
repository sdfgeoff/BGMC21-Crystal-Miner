'''
This module contains often used BGE functions and some mathermatical
operators I think python should include by default

Author: Geoffrey Irons
'''

import os
import sys
import math
import datetime
import logging

import bge
import mathutils

# CONFIGUREABLES
ROOT_SEARCH_FILE = 'Cavex16.blend'  # File used to find project root


# STATIC VARIABLES
START_TIME = datetime.datetime.now()


LOG = logging.getLogger()

def get_from_root(rel=''):
    '''Gets root path of the simulation'''
    blend_path = bge.logic.expandPath('//')
    root_path = blend_path
    while not os.path.isfile(os.path.join(root_path, ROOT_SEARCH_FILE)):
        root_path = os.path.split(root_path)[0]
        if root_path == "/" or len(root_path) < 3:
            LOG.warning("Root path not found, using blend directory")
            root_path = blend_path
            break

    return os.path.join(root_path, rel)


def time_to_string(seconds):
    '''Converts a number of seconds to a string of minutes:seconds'''
    minutes, seconds = divmod(int(seconds), 60)
    print(minutes, seconds)
    return '{:2d}:{:02d}'.format(minutes, seconds)


def get_time_since_start():
    '''Returns the total time the game started running. '''
    timedelta = (datetime.datetime.now() - START_TIME)
    return timedelta


def constrain(num, minimum=-1, maximum=1):
    '''Restricts a number between two values'''
    return min(max(num, minimum), maximum)


def wrap(num, minimum=-1, maximum=1):
    '''wraps the number around'''
    while num > maximum:
        num -= (maximum - minimum) + 1
    while num < minimum:
        num += (maximum - minimum) + 1
    return num


def remap(num, old_min, old_max, new_min, new_max):
    '''Changes a value from one range to another'''
    scale = (new_max - new_min) / (old_max - old_min)
    return (num - old_min) * scale + new_min


def get_scene(name):
    '''Returns scene with specified name or None'''
    scenes = bge.logic.getSceneList()
    scene = None
    for i in scenes:
        if i.name == name:
            scene = i

    return scene


def set_vertex_color(obj, rgba):
    '''Sets all the vertices in a mesh to the specified color'''
    mesh = obj.meshes[0]
    for mat in range(mesh.numMaterials):
        for vert in range(mesh.getVertexArrayLength(mat)):
            mesh.getVertex(mat, vert).color = rgba


def polar2cart(lat, lon):
    '''polar to cartesian coordinate conversion'''
    vect = mathutils.Vector()
    vect.x = math.sin(lat) * math.cos(lon)
    vect.y = math.sin(lat) * math.sin(lon)
    vect.z = math.cos(lat)
    return vect


def cart2polar(vect):
    '''cartesian to polar coordinate conversion'''
    if vect.x == 0 and vect.y == 0 or vect.z == 0:
        return vect.length, 0, 0
    radius = vect.length
    lon = math.acos(vect.x / math.sqrt(vect.x ** 2 + vect.y ** 2))
    if vect.y < 0:
        lon = -lon
    lat_rad = constrain(vect.z / radius, -1, 1)  # Stop Precision Errors
    lat = math.acos(lat_rad)
    return radius, lon, lat


def rgb_to_hsv(red, green, blue):
    '''Returns the hsv components of an rgb color'''
    rgb = mathutils.Color([red, green, blue])
    return rgb.hsv


def lerp(num1, num2, fac):
    ''' interpolates num1 to num2'''
    return (1 - fac) * num1 + fac * num2

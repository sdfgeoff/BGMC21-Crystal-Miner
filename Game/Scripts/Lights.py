'''Handles more lights than you have actual lights'''
import logging

import bge
import defs

LOG = logging.getLogger()


def init(cont):
    '''Adds lights to the lighting manager'''
    LOG.info("Initialized Lighting Manager")
    cont.owner['available_lights'] = generate_light_list(cont)
    cont.owner['used_lights'] = list()
    bge.logic.scheduler.register_function(update_lights, args=[cont])


def update_lights(cont):
    '''Updates the lights so they are the ones closest to the player'''
    available_lights = cont.owner['available_lights']
    used_lights = cont.owner['used_lights']
    positions = generate_best_positions(cont)
    for obj in positions:
        if 'light_obj' not in obj and len(available_lights) > 0:
            light = available_lights.pop()
            light['obj'] = obj
            cont.owner['used_lights'].append(light)
            light.energy = 0.0
            light.worldPosition = obj.worldPosition
            obj['light_obj'] = light

    for light in used_lights[:]:
        obj = light['obj']
        if obj.invalid:  # If the object has been removed
            light.energy = 0.0
            used_lights.remove(light)
            available_lights.append(light)
            continue
        if obj not in positions:
            light.energy = light.energy * 0.5
            if light.energy < 0.01:
                light.energy = 0.0
                used_lights.remove(light)
                available_lights.append(light)
                del obj['light_obj']
            continue

        light.energy = (light.energy * 0.95 + 0.05)
        light.worldPosition = light['obj'].worldPosition


def generate_light_list(cont):
    '''Returns a list of all lights present in a scene'''
    return list(cont.owner.scene.lights)


def generate_best_positions(cont):
    '''Returns the best positions for lights to be in'''
    center_obj = cont.owner  # !
    light_list = list()
    for obj in cont.owner.scene.objects:
        if 'Light' in obj:
            dist = obj.getDistanceTo(center_obj)
            light_list.append((dist, obj))
    return [o[1] for o in sorted(light_list)[:defs.NUM_LIGHTS]]

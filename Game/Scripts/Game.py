'''This is the major script for crystal miner, in charge of creating
and maintaining the scheduler that runs everything else.'''

import bge

import logging
import sys
import os

import Schedular
import Common
import Loader

PRINT_LOG_LEVEL = logging.INFO

LOG_FILENAME = 'Game.log'

LOG = logging.getLogger()
bge.logic.scheduler = Schedular.Schedular()


def start(cont):
    '''Starts the game running by adding necessary scenes, loading
    things in etc.'''
    for sens in cont.sensors:
        if not sens.positive:
            return
    create_logger()
    LOG.info("Starting Game")
    cont.script = __name__ + '.loading'


def loading(cont):
    '''Runs while the game is loading'''
    for sens in cont.sensors:
        if not sens.positive:
            return
    if Loader.load_next(cont) is True:
        cont.script = __name__ + '.update'


def update(cont):
    '''What happens on a given frame'''
    for sens in cont.sensors:
        if not sens.positive:
            return
    bge.logic.scheduler.update()


def create_logger():
    '''Configures the logging logger to write to a file and
    print messages'''
    log_file_path = Common.get_from_root(LOG_FILENAME)
    if os.path.isfile(log_file_path):
        try:
            os.remove(log_file_path)
        except PermissionError:
            LOG.error("Unable to clear last file, log may grow without bound")

    logging.basicConfig(filename=log_file_path, level=logging.DEBUG)

    log_printer = logging.StreamHandler(sys.stdout)
    log_printer.setLevel(PRINT_LOG_LEVEL)
    log_printer.setFormatter(logging.Formatter('%(levelname)s: %(message)s'))
    LOG.addHandler(log_printer)


def restart(cont):
    '''Restarts the game'''
    for sens in cont.sensors:
        if not sens.positive:
            return
    bge.logic.restartGame()
